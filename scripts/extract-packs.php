#!/usr/bin/env php
<?php

$packs = glob(dirname(__DIR__).'/packs/*.db');
foreach ($packs as $pack) {
    $handle = fopen($pack, 'rb');
    $first = true;
    $data = [];

    while ($line = fgets($handle)) {
        $line = trim($line);
        if (!$line) {
            continue;
        }
        $data[] = $line;
    }
    fclose($handle);

    $targetFilename = substr($pack, 0, -3).'.json';
    $handle = fopen($targetFilename, 'wb');
    fwrite($handle, "[\n");
    foreach ($data as $index => $row) {
        if ($index < count($data) - 1) {
            $row .= ',';
        }
        fwrite($handle, "\t".$row."\n");
    }
    fwrite($handle, "]");
    fclose($handle);
}