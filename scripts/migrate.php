#!/usr/bin/env php
<?php

$dataRoot = dirname(__FILE__, 4);
$opt = getopt('', [
    ':source',
    ':destination',
]);

$source = $opt['source'] ?? $dataRoot.'/worlds/shadowrun/data/';
$destination = $opt['destination'] ?? dirname(__DIR__).'/packs/import/';

if (!file_exists($destination)) {
    mkdir($destination);
}

function read($file) {
    $handle = fopen($file, 'rb');
    if (!$handle) {
        echo error_get_last()['message']."\n";
        exit;
    }
    while($line = fgets($handle)) {
        $line = trim($line);
        if (!$line) {
            continue;
        }

        // name, _id, data
        $content = json_decode($line, true);
        $model = [
            'name' => $content['name'] ?? '',
            '_id' => $content['_id'],
            'effects' => [],
            'folder' => null,
            'sort' => 0,
            'permission' => (object)[],
            'flags' => (object)[],
        ];

        if (isset($content['type'])) {
            $model['type'] = $content['type'];
        }
        if (isset($content['data'])) {
            $model['data'] = $content['data'];
        }
        if (isset($content['img'])) {
            $model['img'] = $content['img'];
        }

        if (isset($content['token'])) {
            $model['token'] = $content['token'];
        }

        if (isset($content['items'])) {
            $model['items'] = $content['items'];
        }

        yield $model;
    }
    fclose($handle);
}

function migrate(string $source, string $dest, Closure $migrator)
{
    if (file_exists($dest)) {
        unlink($dest);
    }

    $destHandle = fopen($dest, 'wb');
    if (!$destHandle) {
        echo error_get_last()['message']."\n";
        exit;
    }

    foreach (read($source) as $object) {
        $object = $migrator($object);
        if ($object === null) {
            continue;
        }
        fwrite($destHandle, json_encode($object)."\n");
    }
    fclose($destHandle);
}

function migrateSkill(array $skill): ?array
{
    if ($skill['type'] !== 'skill') {
        return null;
    }
    $skill['data']['attribute'] = strtr($skill['data']['attribute'], ['attributes.' => '']);
    if ($skill['data']['attribute'] === 'Special') {
        $skill['data']['attribute'] = 'Magic'; // I don't have any technomancers so this is easy...
    }
    $skill['data']['specialty'] = $skill['data']['specialisation'] ?? '';
    unset($skill['data']['specialisation']);
    return $skill;
}

function migrateSpirit($actor)
{
    $actor['data']['magic'] = 'magic';
    foreach ($actor['data']['attributes'] as &$attribute) {
        $attribute['value'] = strtr($attribute['calc'], ['F' => '{F}']);
    }

    return $actor;
}

function migrateCharacter($actor)
{
    // Magic
    if ($actor['data']['attributes']['Special']['value']) {
        $actor['data']['magic'] = 'magic'; // I only have mages in my game so this is a safe assumption
        $actor['data']['attributes']['Magic'] = $actor['data']['attributes']['Special'];
    }

    // Meta
    $actor['data']['lifestyle'] = 0;

    // Condition Monitor
    $physical = $actor['data']['condition_physical'];
    $stun = $actor['data']['condition_stun'];

    unset($physical['agony']);
    unset($physical['value']);
    unset($stun['agony']);
    unset($stun['value']);
    $physical['damage'] = 0;
    $stun['damage'] = 0;
    $actor['data']['condition_monitor'] = [
        'physical' => $physical,
        'stun' => $stun,
    ];

    // Initiative
    $actor['data']['initiative'] = [
        'dice' => [
            'value' => $actor['data']['initiative']['dice']
        ],
        'modifier' => [
            'value' => $actor['data']['initiative']['modifier']
        ]
    ];

    return $actor;
}

function migrateWeapon($item)
{
    // pick weapon type
    if (!$item['data']['attack_ratings']['near']) {
        $item['type'] = 'meleeWeapon';
    } else {
        $item['type'] = 'rangedWeapon';
    }

    $item['data']['attack_rating'] = implode('/', $item['data']['attack_ratings']);
    $item['data']['damage_value'] = $item['data']['dv'];

    // todo - fire modes
    return $item;
}

function migrateCyberware($item)
{
    $item['data']['modifiers'] = $item['data']['effects'];
    $item['data']['effects'] = '';

    // todo - still need to calculate essence cost
    return $item;
}

function migrateEquipment($item, $attributes = ['rating' => 'Rating', 'capacity' => 'Capacity'])
{
    $info = [];
    foreach ($attributes as $prop => $name) {
        if ($item['data'][$prop]) {
            $info[] = $name.' '.$item['data'][$prop];
        }
    }

    $stats = implode(', ', $info);
    if ($stats) {
        $stats .= "\n\n";
    }
    $item['type'] = 'equipment';
    $item['data']['description'] = $stats.($item['data']['description'] ?? '');

    return $item;
}

function migrateExplosive($item)
{
    $info = [
        "Blast: {$item['data']['blast']}",
        "DV Ground Zero: {$item['data']['dv']['gz']}",
        "DV Close: {$item['data']['dv']['close']}",
        "DV Near: {$item['data']['dv']['near']}",
    ];
    $item['data']['description'] = implode("\n", $info)."\n\n".$item['data']['description'];
    $item['type'] = 'equipment';
    return $item;
}

function migrateArmour($item)
{
    $item['data']['defense_rating'] = $item['data']['defense'];
    return $item;
}

function migrateContact($item)
{
    return $item;
}

migrate($source.'journal.db', $destination.'journal.db', static fn($object) => $object);
migrate($source.'items.db', $destination.'items.db', static fn($object) => migrateSkill($object));
migrate($source.'actors.db', $destination.'actors.db', static function($actor) {

    if (!isset($actor['type'])) {
        return null;
    }
    if ($actor['type'] === 'spirit') {
        $actor = migrateSpirit($actor);
    } elseif ($actor['type'] === 'character') {
        $actor = migrateCharacter($actor);
    } else {
        echo "Unknown character type '{$actor['type']}' ({$actor['name']})\n";
        return null;
    }

    // Items
    $items = [];
    foreach ($actor['items'] as $item) {
        switch($item['type']) {
            case 'skill':
                $items[] = migrateSkill($item);
                break;
            case 'vehicle': // Dr1zzt
            case 'software': // Dr1zzt
            case 'cyberdeck': // Dr1zzt
            case 'effect': // not used on live
            case 'power': // devil rats
            case 'spell': // radiation shaman
            case 'metamagic': // radiation shaman
                // no-op (i.e. skip)
                break;
            case 'weapon':
                $items[] = migrateWeapon($item);
                break;
            case 'quality':
                $items[] = $item;
                break;
            case 'cyberware':
                $items[] = migrateCyberware($item);
                break;
            case 'equipment':
                $items[] = migrateEquipment($item);
                break;
            case 'commlink':
                $items[] = migrateEquipment($item, ['rating' => 'Rating', 'data_processing' => 'DP', 'firewall' => 'F', 'program_slots' => 'Programs']);
                break;
            case 'identity':
                $items[] = migrateEquipment($item, ['rating' => 'Rating', 'official' => 'Real', 'licenses' => 'Licenses:', 'notes' => 'Notes:']);
                break;
            case 'explosives':
                $items[] = migrateExplosive($item);
                break;
            case 'contact':
                $items[] = migrateContact($item);
                break;
            case 'armour':
                $items[] = migrateArmour($item);
                break;
            case '___INTROSPECT___':
                echo $actor['name']."\n";
                break;
            default:
                echo "Unknown item type '{$item['type']}'\n";
        }
    }
    $actor['items'] = $items;

    return $actor;
});
// actors.db