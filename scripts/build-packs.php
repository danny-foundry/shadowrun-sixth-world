#!/usr/bin/env php
<?php

$packs = glob(dirname(__DIR__).'/packs/*.json');
foreach ($packs as $pack) {
    $data = json_decode(file_get_contents($pack), true);
    $targetFilename = substr($pack, 0, -5).'.db';
    $handle = fopen($targetFilename, 'wb');

    foreach ($data as $row) {
        fwrite($handle, json_encode($row)."\n");
    }
    fclose($handle);
}