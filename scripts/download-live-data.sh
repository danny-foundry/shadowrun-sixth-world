#!/usr/bin/env bash

cd "$(dirname "$0")/../../../worlds/enter-the-shadows"
sudo systemctl stop foundryvtt
sudo chmod -R 777 ./
sudo chmod -R 777 ../../assets/
rsync -r --stats --progress foundry@twoflower.cain.mobi:foundry-data/Data/worlds/enter-the-shadows/ ./
rsync -r --stats --progress foundry@twoflower.cain.mobi:foundry-data/Data/assets/ ../../assets/
rm -f data/users.db
sudo systemctl start foundryvtt
