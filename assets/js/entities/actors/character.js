import BaseActor from "./base-actor.js";
import {object_get, object_set} from "../../helpers/objects.js";

export default class Character extends BaseActor
{
    setDefaults() {
        if (!object_get(this.actor.data.data, ['condition_monitor', 'physical', 'max'])) {
            object_set(this.actor.data.data, ['condition_monitor', 'physical', 'max'], 10);
        }
        if (!object_get(this.actor.data.data, ['condition_monitor', 'stun', 'max'])) {
            object_set(this.actor.data.data, ['condition_monitor', 'stun', 'max'], 10);
        }

        if (!object_get(this.actor.data.data, ['defense_rating'])) {
            object_set(this.actor.data.data, ['defense_rating', 'value'], 0);
            object_set(this.actor.data.data, ['defense_rating', 'modifier'], 0);
        }

        if (typeof this.actor.data.data.initiative.dice !== 'object') {
            this.actor.data.data.initiative.dice = { value: this.actor.data.data.initiative.dice, modifier: 0 }
        }

        if (typeof this.actor.data.data.initiative.modifier !== 'object') {
            this.actor.data.data.initiative.modifier = { value: this.actor.data.data.initiative.modifier, modifier: 0 }
        }


        if (!this.actor.data.data.condition_monitor.physical.divisor) {
            this.actor.data.data.condition_monitor.physical.divisor = 3;
        }
        if (!this.actor.data.data.condition_monitor.stun.divisor) {
            this.actor.data.data.condition_monitor.stun.divisor = 3;
        }

        this.actor.data.data.settings.rigger = this.actor.data.data.settings.rigger || false;
        if (this.actor.data.data.rigging === undefined) {
            this.actor.data.data.rigging = {
                drone: null,
            };
        }
    }
    
    calculateAttributes() {
        let essence = this.actor.items.reduce((total, item) => total + (item.essence_cost || 0), 0);
        let hole = this.actor.data.data.attributes.Essence.hole;
        let lost = this.actor.data.data.attributes.Essence.lost;
        essence = Math.max(essence - hole, 0);


        this.actor.data.data.attributes.Essence.value = 6 - lost;
        this.actor.data.data.attributes.Essence.modifier = -Math.ceil(essence);

        Object.keys(this.actor.data.data.attributes).forEach((attr) => {
            this.actor.data.data.attributes[attr].modifier = Math.min(4, this.actor.data.data.attributes[attr].modifier || 0);
            this.actor.data.data.attributes[attr].total = this.actor.data.data.attributes[attr].value + this.actor.data.data.attributes[attr].modifier;
        });

        this.actor.data.data.initiative.dice.modifier = Math.min(4, this.actor.data.data.initiative.dice.modifier || 0);
        this.actor.data.data.initiative.dice.total = this.actor.data.data.initiative.dice.value + this.actor.data.data.initiative.dice.modifier;

        this.actor.data.data.initiative.modifier.value = this.actor.getCharacteristic('attributes.Reaction') + this.actor.getCharacteristic('attributes.Intuition');
        this.actor.data.data.initiative.modifier.modifier = this.actor.data.data.initiative.modifier.modifier || 0;
        this.actor.data.data.initiative.modifier.total = this.actor.data.data.initiative.modifier.value + this.actor.data.data.initiative.modifier.modifier;
    }
    
    calculateConditionMonitors() {
        const physicalRemaining =
            object_get(this.actor.data.data, ['condition_monitor', 'physical', 'max'], 0) -
            object_get(this.actor.data.data, ['condition_monitor', 'physical', 'damage'], 0);

        const stunRemaining =
            object_get(this.actor.data.data, ['condition_monitor', 'stun', 'max'], 0) -
            object_get(this.actor.data.data, ['condition_monitor', 'stun', 'damage'], 0);

        object_set(this.actor.data.data, ['condition_monitor', 'physical', 'value'], physicalRemaining);
        object_set(this.actor.data.data, ['condition_monitor', 'stun', 'value'], stunRemaining);

        const physicalDivisor = this.actor.data.data.condition_monitor.physical.divisor;
        const stunDivisor = this.actor.data.data.condition_monitor.stun.divisor;

        const physicalPenalty = -Math.floor(object_get(this.actor.data.data, ['condition_monitor', 'physical', 'damage'], 0) / physicalDivisor);
        const stunPenalty = -Math.floor(object_get(this.actor.data.data, ['condition_monitor', 'stun', 'damage'], 0) / stunDivisor);
        object_set(this.actor.data.data, ['condition_monitor', 'physical', 'penalty'], physicalPenalty);
        object_set(this.actor.data.data, ['condition_monitor', 'stun', 'penalty'], stunPenalty);
    }

    calculateDerivedData() {
        const dodge = this.actor._derive(['attributes.Reaction', 'attributes.Intuition']);
        const soak = this.actor._derive(['attributes.Body']);
        const lift = this.actor._derive(['attributes.Body', 'attributes.Willpower']);
        const sprint = this.actor._derive(['attributes.Agility', 'skills.Athletics']);
        const composure = this.actor._derive(['attributes.Willpower', 'attributes.Charisma']);
        const intentions = this.actor._derive(['attributes.Willpower', 'attributes.Intuition']);
        const memory = this.actor._derive(['attributes.Logic', 'attributes.Intuition']);

        const drainAttribute = object_get(this.actor.data.data, ['settings', 'drain_attribute']);
        const drain = this.actor._derive(['attributes.Willpower', `attributes.${drainAttribute}`]);
        const karmaAvailable = this.actor.data.data.karma.earnt - this.actor.data.data.karma.spent;

        this.actor.data.data.derived = {
            dodge: dodge,
            soak: soak,
            lift: lift,
            sprint: sprint,
            composure: composure,
            intentions: intentions,
            memory: memory,
            drain: drain,
        };

        let defenseRating = this.actor.items
            .filter((item) => item.type === 'armour')
            .reduce((total, item) => total + parseInt(item.defense_rating), this.actor._derive(['attributes.Body']));

        object_set(this.actor.data.data, ['karma', 'available'], karmaAvailable);
        object_set(this.actor.data.data, ['defense_rating', 'value'], defenseRating);
        object_set(this.actor.data.data, ['defense_rating', 'total'], this.actor._derive(['defense_rating.value', 'defense_rating.modifier']));
    }

    attributes() {
        return ['Body', 'Agility', 'Reaction', 'Strength', 'Willpower', 'Logic', 'Intuition', 'Charisma', 'Edge', 'Magic', 'Resonance'];
    }

    skills() {
        return this.actor.itemsByType()['skill'];
    }

    updated(changed, options, userId) {
        let drone = changed.data?.rigging?.drone;
        if (drone !== undefined) {
            if (this.last_drone) {
                Actors.instance.get(this.last_drone).sheet.render();
            }
            if (drone) {
                Actors.instance.get(drone).sheet.render();
            }

            this.last_drone = drone;
        }
    }


    dicePool(characteristic, pool) {
        if (characteristic === 'rigging') {
            this._riggingDicePool(pool);
            return true;
        }

        return false;
    }

    _riggingDicePool(dicePool) {
        let drone = Actors.instance.get(this.actor.data.data.rigging.drone);
        let rcu = this.actor.itemsByType()['cyberware'].find((ware) => ware.id === this.actor.data.data.rigging.rcu);

        if (rcu) {
            dicePool.push({
                text: 'Rigger Control Unit',
                value: rcu.data.data.rating,
                source: 'Rigging',
                negative: false,
            });
        }

        dicePool.push({
            text: 'Speed Penalty',
            value: drone.data.data.speed_penalty,
            source: 'Rigging',
            negative: true,
        });


        let handlingThreshold = drone.data.data.attributes[drone.data.data.handling]?.total || 0;
        if (handlingThreshold) {
            dicePool.push({
                text: `Handling Threshold ${handlingThreshold}`,
                source: 'Rigging',
                negative: false,
            });
        }
    }
}