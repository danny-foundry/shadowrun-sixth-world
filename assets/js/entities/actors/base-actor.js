export default class BaseActor
{
    constructor(actor) {
        this.actor = actor;
    }

    setDefaults() {

    }

    calculateConditionMonitors() {

    }

    calculateAttributes() {

    }

    calculateDerivedData() {

    }

    woundPenalties() {
        const physical = this.actor.data.data.condition_monitor?.physical?.penalty || 0;
        const stun = this.actor.data.data.condition_monitor?.stun?.penalty || 0;

        return physical + stun;
    }

    updated(changed, options, userId) {

    }

    attributes() {

    }

    skills() {

    }

    dicePool(characteristic, pool) {
        return false;
    }
}