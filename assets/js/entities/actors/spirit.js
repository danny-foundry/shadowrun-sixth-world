import BaseActor from "./base-actor.js";
import {object_get, object_set} from "../../helpers/objects.js";

export default class Spirit extends BaseActor
{
    setDefaults() {
        if (!object_get(this.actor.data.data, ['condition_monitor', 'physical', 'max'])) {
            object_set(this.actor.data.data, ['condition_monitor', 'physical', 'max'], 10);
        }
        if (!object_get(this.actor.data.data, ['condition_monitor', 'stun', 'max'])) {
            object_set(this.actor.data.data, ['condition_monitor', 'stun', 'max'], 10);
        }

        if (!object_get(this.actor.data.data, ['defense_rating'])) {
            object_set(this.actor.data.data, ['defense_rating', 'value'], 0);
            object_set(this.actor.data.data, ['defense_rating', 'modifier'], 0);
        }

        if (typeof this.actor.data.data.initiative.dice !== 'object') {
            this.actor.data.data.initiative.dice = { value: this.actor.data.data.initiative.dice, modifier: 0 }
        }

        if (typeof this.actor.data.data.initiative.modifier !== 'object') {
            this.actor.data.data.initiative.modifier = { value: this.actor.data.data.initiative.modifier, modifier: 0 }
        }


        if (!this.actor.data.data.condition_monitor.physical.divisor) {
            this.actor.data.data.condition_monitor.physical.divisor = 3;
        }
        if (!this.actor.data.data.condition_monitor.stun.divisor) {
            this.actor.data.data.condition_monitor.stun.divisor = 3;
        }

        this.data.data.magic = 'magic';
    }

    calculateConditionMonitors() {
        const physicalRemaining =
            object_get(this.actor.data.data, ['condition_monitor', 'physical', 'max'], 0) -
            object_get(this.actor.data.data, ['condition_monitor', 'physical', 'damage'], 0);

        const stunRemaining =
            object_get(this.actor.data.data, ['condition_monitor', 'stun', 'max'], 0) -
            object_get(this.actor.data.data, ['condition_monitor', 'stun', 'damage'], 0);

        object_set(this.actor.data.data, ['condition_monitor', 'physical', 'value'], physicalRemaining);
        object_set(this.actor.data.data, ['condition_monitor', 'stun', 'value'], stunRemaining);

        const physicalDivisor = this.actor.data.data.condition_monitor.physical.divisor;
        const stunDivisor = this.actor.data.data.condition_monitor.stun.divisor;

        const physicalPenalty = -Math.floor(object_get(this.actor.data.data, ['condition_monitor', 'physical', 'damage'], 0) / physicalDivisor);
        const stunPenalty = -Math.floor(object_get(this.actor.data.data, ['condition_monitor', 'stun', 'damage'], 0) / stunDivisor);
        object_set(this.actor.data.data, ['condition_monitor', 'physical', 'penalty'], physicalPenalty);
        object_set(this.actor.data.data, ['condition_monitor', 'stun', 'penalty'], stunPenalty);
    }

    calculateAttributes() {
        Object.keys(this.actor.data.data.attributes).forEach((attr) => {
            // todo - do this.actor without using eval
            const attribute = this.actor.data.data.attributes[attr].value;
            try {
                let value = attribute ? eval(('' + attribute).replace('{F}', this.actor.data.data.force || 1)) : 1;
                if (value < 1) {
                    value = 1;
                }
                this.actor.data.data.attributes[attr].modifier = Math.min(4, this.actor.data.data.attributes[attr].modifier || 0);
                this.actor.data.data.attributes[attr].total = value + this.actor.data.data.attributes[attr].modifier;
            } catch(err) {
                this.actor.data.data.attributes[attr].total = 0;
            }
        });

        this.actor.data.data.initiative.dice.modifier = Math.min(4, this.actor.data.data.initiative.dice.modifier || 0);
        this.actor.data.data.initiative.dice.total = this.actor.data.data.initiative.dice.value + this.actor.data.data.initiative.dice.modifier;

        this.actor.data.data.initiative.modifier.value = this.actor.getCharacteristic('attributes.Reaction') + this.actor.getCharacteristic('attributes.Intuition');
        this.actor.data.data.initiative.modifier.modifier = this.actor.data.data.initiative.modifier.modifier || 0;
        this.actor.data.data.initiative.modifier.total = this.actor.data.data.initiative.modifier.value + this.actor.data.data.initiative.modifier.modifier;
    }

    calculateDerivedData() {
        const dodge = this.actor._derive(['attributes.Reaction', 'attributes.Intuition']);
        const soak = this.actor._derive(['attributes.Body']);
        const lift = this.actor._derive(['attributes.Body', 'attributes.Willpower']);
        const sprint = this.actor._derive(['attributes.Agility', 'skills.Athletics']);
        const composure = this.actor._derive(['attributes.Willpower', 'attributes.Charisma']);
        const intentions = this.actor._derive(['attributes.Willpower', 'attributes.Intuition']);
        const memory = this.actor._derive(['attributes.Logic', 'attributes.Intuition']);

        const drainAttribute = object_get(this.actor.data.data, ['settings', 'drain_attribute']);
        const drain = this.actor._derive(['attributes.Willpower', `attributes.${drainAttribute}`]);
        const karmaAvailable = this.actor.data.data.karma.earnt - this.actor.data.data.karma.spent;

        this.actor.data.data.derived = {
            dodge: dodge,
            soak: soak,
            lift: lift,
            sprint: sprint,
            composure: composure,
            intentions: intentions,
            memory: memory,
            drain: drain,
        };

        let defenseRating = this.actor.items
            .filter((item) => item.type === 'armour')
            .reduce((total, item) => total + parseInt(item.defense_rating), this.actor._derive(['attributes.Body']));

        object_set(this.actor.data.data, ['karma', 'available'], karmaAvailable);
        object_set(this.actor.data.data, ['defense_rating', 'value'], defenseRating);
        object_set(this.actor.data.data, ['defense_rating', 'total'], this.actor._derive(['defense_rating.value', 'defense_rating.modifier']));
    }

    attributes() {
        return ['Body', 'Agility', 'Reaction', 'Strength', 'Willpower', 'Logic', 'Intuition', 'Charisma', 'Edge', 'Magic', 'Resonance'];
    }

    skills() {
        return this.actor.itemsByType()['skills'];
    }
}