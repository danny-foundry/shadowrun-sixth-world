import BaseActor from "./base-actor.js";
import {object_get, object_set} from "../../helpers/objects.js";

export default class Drone extends BaseActor
{
    calculateAttributes() {
        if (!object_get(this.actor.data.data, ['condition_monitor', 'physical', 'max'])) {
            object_set(this.actor.data.data, ['condition_monitor', 'physical', 'max'], 10);
        }

        Object.keys(this.actor.data.data.attributes).forEach((attr) => {
            this.actor.data.data.attributes[attr].modifier = Math.min(4, this.actor.data.data.attributes[attr].modifier || 0);
            this.actor.data.data.attributes[attr].total = this.actor.data.data.attributes[attr].value + this.actor.data.data.attributes[attr].modifier;
        });
    }

    calculateConditionMonitors() {
        const physicalRemaining =
            object_get(this.actor.data.data, ['condition_monitor', 'physical', 'max'], 0) -
            object_get(this.actor.data.data, ['condition_monitor', 'physical', 'damage'], 0);

        object_set(this.actor.data.data, ['condition_monitor', 'physical', 'value'], physicalRemaining);

        const physicalDivisor = 3;

        const physicalPenalty = -Math.floor(object_get(this.actor.data.data, ['condition_monitor', 'physical', 'damage'], 0) / physicalDivisor);
        object_set(this.actor.data.data, ['condition_monitor', 'physical', 'penalty'], physicalPenalty);
    }

    calculateDerivedData() {
        this.actor.data.data.attributes.OnroadHandling.modifier += this.actor.data.data.condition_monitor.physical.penalty*-1;
        this.actor.data.data.attributes.OnroadHandling.total += this.actor.data.data.condition_monitor.physical.penalty*-1;

        this.actor.data.data.attributes.OffroadHandling.modifier += this.actor.data.data.condition_monitor.physical.penalty*-1;
        this.actor.data.data.attributes.OffroadHandling.total += this.actor.data.data.condition_monitor.physical.penalty*-1;

        this.actor.data.data.speed_penalty = -Math.floor(this.actor.data.data.current_speed / this.actor.data.data.attributes.SpeedInterval.total);

        this.actor.data.data.initiative = {
            dice: {
                total: 3,
            },
            modifier: {
                total: this.actor.data.data.attributes.Pilot.total * 2,
            }
        };
    }

    attributes() {
        return [
            "OnroadHandling",
            "OffroadHandling",
            "Acceleration",
            "SpeedInterval",
            "TopSpeed",
            "Body",
            "Armor",
            "Pilot",
            "Sensor",
            "Seats"
        ];
    }

    skills() {
        return this.actor.itemsByType()['autosoft'] || [];
    }

    updated() {
        let rigger = this.actor.linkableActorsByType('character')
            .find((char) => char.data.data.rigging?.drone === this.actor.id);

        if (rigger) {
            rigger.sheet.render();
        }
    }

    woundPenalties() {
        return 0;
    }

    dicePool(characteristic, pool) {
        switch (characteristic) {
            case 'handling':
                pool.push({
                    text: `Handling (${this.actor.data.data.attributes[this.actor.data.data.handling].total})`,
                    negative: false,
                });
                return true;
            case 'speed':
                pool.push({
                    text: 'Speed',
                    value: this.actor.data.data.speed_penalty,
                    negative: true,
                });
                return true;
        }

        return false;
    }
}