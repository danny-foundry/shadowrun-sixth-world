import {parseDicePool} from "../helpers/dice.js";
import {object_get, object_set} from "../helpers/objects.js";
import {applyEffects, triggerEffects, getTotalModifier} from "../effects/effects.js";
import Character from "./actors/character.js";
import Spirit from "./actors/spirit.js";
import Drone from "./actors/drone.js";

export default class ShadowrunActor extends Actor
{
    constructor(data, context) {
        super(data, context);
    }

    // object setup - before items and effects
    prepareBaseData() {

    }

    // set any computed attributes for this entity, including from items and effects
    prepareDerivedData() {
        switch(this.type) {
            case 'spirit':
                this.instance = new Spirit(this);
                break;
            case 'drone':
                this.instance = new Drone(this);
                break;
            default:
                this.instance = new Character(this);
        }

        this.instance.setDefaults();
        this.instance.calculateConditionMonitors();
        applyEffects(this);
        this.instance.calculateAttributes();
        this.instance.calculateDerivedData();
        // todo - need to handle spirit skills (ranks = Force)
    }

    _derive(characteristics) {
        return characteristics.reduce((previous, characteristic) => previous + this.getCharacteristic(characteristic), 0);
    }

    woundPenalties() {
        return this.instance.woundPenalties();
    }

    // Returns all actors that are owned by the same users as this actor
    linkableActorsByType(type) {
        const owners = Object.entries(this.data.permission)
            .filter((perm) => perm[1] >= 3) // owner
            .filter((perm) => perm[0] !== 'default') // ignore default
            .filter((perm) => Users.instance.get(perm[0])?.data?.role < 4) // exclude GM's
            .map((perm) => perm[0]);

        return Array.from(Actors.instance)
            .filter((actor) => actor.type === type)
            .filter((actor) => actor.id !== this.id)
            .filter((actor) => {
                for (let user of owners) {
                    if ((actor.data.permission[user] || 0) < 3) {
                        return false;
                    }
                }
                return true;
            })
    }

    itemLookupsByType() {
        let types = {};

        this.items.forEach((item) => {
            if (!types[item.type]) {
                types[item.type] = {};
            }

            types[item.type][item.name] = item;
        });

        return types;
    }

    isTrained(skillName) {
        return this.itemsByType()['skill']?.filter((skill) => skill.data.name === skillName).length > 0;
    }

    isUntrained(skillName) {
        return !this.isTrained(skillName);
    }

    skills() {
        return this.instance.skills();
    }

    attributes() {
        return this.instance.attributes();
    }

    itemsByType() {
        let types = {};

        this.items
            .forEach((item) => {
                if (!types[item.type]) {
                    types[item.type] = [];
                }

                types[item.type].push(item);
            });

        Object.values(types).map((items) => {
            return items.sort((a, b) => {
                const aName = a.data.name.toLowerCase();
                const bName = b.data.name.toLowerCase();

                if (aName < bName) {
                    return -1;
                }
                if (aName > bName) {
                    return 1;
                }
                return 0;
            });
        });

        return types;
    }

    setModifier(characteristic, modifier) {
        let name = characteristic.split('.');
        let type = name.shift();

        if (this.data.data[type] !== undefined) {
            object_set(this.data.data, [type, ...name, 'modifier'], modifier);
        } else {
            if (type.endsWith('s')) {
                type = type.substring(0, type.length - 1);
            }
            let itemName = name.shift();

            let item = this.itemLookupsByType()[type]?.[itemName] || { data: {} };
            if (!name.length) {
                item.data.data.modifier = modifier;
            } else {
                object_set(item.data.data, [name, 'modifier'], modifier);
            }
        }
    }

    getModifier(characteristic) {
        let name = characteristic.split('.');
        let type = name.shift();
        let value;

        if (this.data.data[type]) {
            value = object_get(this.data.data, [type, ...name]);
        } else {
            if (type.endsWith('s')) {
                type = type.substring(0, type.length - 1);
            }
            let itemName = name.shift();

            let item = this.itemLookupsByType()[type]?.[itemName] || { data: {} };
            if (!name.length) {
                value = item.data.data;
            } else {
                value = object_get(item.data.data, name);
            }
        }

        if (typeof value === 'object') {
            value = value.modifier || 0;
        }

        return value;
    }

    getCharacteristic(characteristic) {
        let name = characteristic.split('.');
        let type = name.shift();
        let value;

        if (type === 'drone') {
            let drone = Actors.instance.get(this.data.data.rigging.drone);
            if (!drone) {
                return 0;
            }

            return drone.getCharacteristic(name.join('.'));
        }

        if (this.data.data[type]) {
            value = object_get(this.data.data, [type, ...name]);
        } else {
            if (type.endsWith('s')) {
                type = type.substring(0, type.length - 1);
            }
            let itemName = name.shift();

            let item = this.itemLookupsByType()[type]?.[itemName] || { data: {} };
            if (!name.length) {
                value = item.data.data;
            } else {
                value = object_get(item.data.data, name);
            }
        }

        if (value === undefined) {
            value = 0;
        }
        if (typeof value === 'object') {
            value = value.total || value.ranks || 0;
        }

        return value;
    }


    // todo - centralise this somewhere useful?
    dicePool(pool) {
        let dicePool = [];

        for (let element of parseDicePool(pool)) {
            let negative = element[0] === '-';
            let value = 0;
            let text = '';

            element = element.substring(1).trim();
            element = element.replace(/_/g, ' ');

            let source = element;

            if (this.instance.dicePool(element, dicePool)) {
                continue;
            }

            if (isNaN(element)) {
                value = this.getCharacteristic(element);
                text = element.split('.').pop();
            } else {
                value = parseInt(element);
                text = 'Modifier';
                source = '';
            }

            if (negative) {
                value = value * -1;
            }
            dicePool.push({
                text: text,
                value: value,
                source: source,
                negative: negative,
            });
        }

        triggerEffects(this, 'roll', dicePool);

        return dicePool;
    }

    _initialize()
    {
        super._initialize();
        this.last_drone = this.data?.data?.rigging?.drone;
    }

    _onUpdate(changed, options, userId)
    {
        super._onUpdate(changed, options, userId);

        this.instance.updated(changed, options, userId);
    }
}