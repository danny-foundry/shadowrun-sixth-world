export default class ShadowrunItem extends Item
{
    // object setup - before items and effects
    prepareBaseData() {

    }

    // set any computed attributes for this entity, including from items and effects
    prepareDerivedData() {
        this.data.data.derived = {
            is_specialty: this.data.data.specialised === 'specialty',
            is_expertise: this.data.data.specialised === 'expertise',
        };

        if (this.type === 'spell' || this.type === 'power') {
            this._prepareSpellDerivedData();
        }
    }

    _prepareSpellDerivedData() {
        this.data.data.derived.is_combat_spell = this.data.data.type.startsWith('combat-');
    }

    get essence_cost() {
        if (this.data.type !== 'bioware' && this.data.type !== 'cyberware') {
            return null;
        }

        return this.data.data.essence;
    }

    get defense_rating() {
        if (this.data.type !== 'armour') {
            return null;
        }

        return this.data.data.defense_rating;
    }

    get attack_rating() {
        const isCombatType = this.data.data.type && (this.data.data.type === 'combat' || this.data.data.type.startsWith('combat-'));
        const isSpellLike = this.data.type === 'spell' || this.data.type === 'power';

        if (isSpellLike) {
            if (!this.actor) {
                return null;
            }

            if (!isCombatType) {
                return null;
            }

            const drainAttributeName = this.actor.data.data.settings?.drain_attribute;
            const drainAttributeValue = this.actor.getCharacteristic(`attributes.${drainAttributeName}`);
            return this.actor.getCharacteristic('attributes.Magic') + drainAttributeValue;
        }

        return this.data.data.attack_rating;
    }

    get damage_value() {
        const isCombatType = this.data.data.type && (this.data.data.type === 'combat' || this.data.data.type.startsWith('combat-'));
        const isSpellLike = this.data.type === 'spell' || this.data.type === 'power';

        if (isSpellLike && isCombatType) {
            if (!this.actor) {
                return null;
            }

            if (this.data.data.type === 'combat-direct') {
                return 0;
            } else {
                return '' + Math.ceil(this.actor.getCharacteristic('attributes.Magic') / 2) + this.data.data.damage_type;
            }
        }
        return this.data.data.damage_value;
    }

    get attack_skill() {
        const isCombatType = this.data.data.type && (this.data.data.type === 'combat' || this.data.data.type.startsWith('combat-'));
        const isSpellLike = this.data.type === 'spell' || this.data.type === 'power';

        if (isSpellLike && isCombatType) {
            if (!this.actor) {
                return null;
            }
            return this.actor.itemsByType()['skill'].filter((skill) => skill.data.name === 'Sorcery')[0]?.id;
        }

        return this.data.data.skill;
    }

    get casting_roll() {
        const skill = this.actor?.items.filter((item) => item.type === 'skill' && item.data.name === 'Sorcery')[0] || null;
        if (!skill) {
            return '';
        }

        return `attributes.${skill.data.data.attribute}+skills.${skill.data.name}`;
    }

    get attack_roll() {
        const skillId = this.attack_skill;
        if (this.actor && skillId) {
            const skill = this.actor.itemsByType()['skill'].find((skill) => skill.id === skillId);
            if (skill) {
                return `attributes.${skill.data.data.attribute}+skills.${skill.data.name}`;
            }
        }

        return '';
    }
}