import ShadowrunActor from "./entities/actor.js";
import ShadowrunItem from "./entities/item.js";
import ShadowrunCharacterSheet from "./sheets/actors/character.js";
import ShadowrunSpiritSheet from "./sheets/actors/spirit.js";
import ShadowrunDroneSheet from "./sheets/actors/drone.js";
import EffectSheet from "./sheets/items/effect.js";
import SkillSheet from "./sheets/items/skill.js";
import AutosoftSheet from "./sheets/items/autosoft.js";
import QualitySheet from "./sheets/items/quality.js";
import MeleeWeaponSheet from "./sheets/items/melee-weapon.js";
import RangedWeaponSheet from "./sheets/items/ranged-weapon.js";
import SpellSheet from "./sheets/items/spell.js";
import EquipmentSheet from "./sheets/items/equipment.js";
import CyberwareSheet from "./sheets/items/cyberware.js";
import BiowareSheet from "./sheets/items/bioware.js";
import PowerSheet from "./sheets/items/power.js";
import ArmourSheet from "./sheets/items/armour.js";
import ContactSheet from "./sheets/items/contact.js";
import HandlebarsSetup from './helpers/handlebars.js';

Hooks.once('init', () => {
    CONFIG.Actor.documentClass = ShadowrunActor;
    CONFIG.Item.documentClass = ShadowrunItem;

    Actors.unregisterSheet("core", ActorSheet);
    Items.unregisterSheet("core", ItemSheet);

    Actors.registerSheet("shadowrun", ShadowrunCharacterSheet, {
        types: ["character"],
        makeDefault: true,
        label: "Character",
    });

    Actors.registerSheet("shadowrun", ShadowrunSpiritSheet, {
        types: ["spirit"],
        label: "Spirit",
    });

    Actors.registerSheet("shadowrun", ShadowrunDroneSheet, {
        types: ["drone"],
        label: "Drone",
    });

    Items.registerSheet("shadowrun", EffectSheet, {
        types: ["effect"],
        label: "Effects"
    });

    Items.registerSheet("shadowrun", SkillSheet, {
        types: ["skill"],
        label: "Skills"
    });

    Items.registerSheet("shadowrun", AutosoftSheet, {
        types: ["autosoft"],
        label: "Autosoft"
    });

    Items.registerSheet("shadowrun", QualitySheet, {
        types: ["quality"],
        label: "Quality"
    });

    Items.registerSheet("shadowrun", MeleeWeaponSheet, {
        types: ["meleeWeapon"],
        label: "Melee Weapon"
    });

    Items.registerSheet("shadowrun", RangedWeaponSheet, {
        types: ["rangedWeapon"],
        label: "Ranged Weapon"
    });

    Items.registerSheet("shadowrun", SpellSheet, {
        types: ["spell"],
        label: "Spell"
    });

    Items.registerSheet("shadowrun", EquipmentSheet, {
        types: ["equipment"],
        label: "Equipment"
    });

    Items.registerSheet("shadowrun", CyberwareSheet, {
        types: ["cyberware"],
        label: "Cyberware"
    });

    Items.registerSheet("shadowrun", BiowareSheet, {
        types: ["bioware"],
        label: "Bioware"
    });

    Items.registerSheet("shadowrun", PowerSheet, {
        types: ["power"],
        label: "Power"
    });

    Items.registerSheet("shadowrun", ArmourSheet, {
        types: ["armour"],
        label: "Armour"
    });

    Items.registerSheet("shadowrun", ContactSheet, {
        types: ["contact"],
        label: "Contact"
    });

    loadTemplates([
        'systems/shadowrun-sixth-world/assets/templates/character/attributes.html',
        'systems/shadowrun-sixth-world/assets/templates/character/armour.html',
        'systems/shadowrun-sixth-world/assets/templates/character/combat.html',
        'systems/shadowrun-sixth-world/assets/templates/character/contacts.html',
        'systems/shadowrun-sixth-world/assets/templates/character/debug.html',
        'systems/shadowrun-sixth-world/assets/templates/character/effects.html',
        'systems/shadowrun-sixth-world/assets/templates/character/gear.html',
        'systems/shadowrun-sixth-world/assets/templates/character/qualities.html',
        'systems/shadowrun-sixth-world/assets/templates/character/powers.html',
        'systems/shadowrun-sixth-world/assets/templates/character/rigging.html',
        'systems/shadowrun-sixth-world/assets/templates/character/settings.html',
        'systems/shadowrun-sixth-world/assets/templates/character/skills.html',
        'systems/shadowrun-sixth-world/assets/templates/character/social.html',
        'systems/shadowrun-sixth-world/assets/templates/character/spells.html',
        'systems/shadowrun-sixth-world/assets/templates/character/weapons.html',
        'systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/armour.html',
        'systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/meleeWeapon.html',
        'systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/rangedWeapon.html',
        'systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/spell.html',
        'systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/equipment.html',
        'systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/bioware.html',
        'systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/cyberware.html',

        'systems/shadowrun-sixth-world/assets/templates/drone/attributes.html',
        'systems/shadowrun-sixth-world/assets/templates/drone/autosofts.html',
        'systems/shadowrun-sixth-world/assets/templates/drone/buttons.html',
        'systems/shadowrun-sixth-world/assets/templates/drone/debug.html',
        'systems/shadowrun-sixth-world/assets/templates/drone/stats.html',
        'systems/shadowrun-sixth-world/assets/templates/drone/weapons.html',

        'systems/shadowrun-sixth-world/assets/templates/spirit/attributes.html',

        'systems/shadowrun-sixth-world/assets/templates/items/weapon.html',

        'systems/shadowrun-sixth-world/assets/templates/dialog/advanced-roll.html',
    ]);

    HandlebarsSetup();
});