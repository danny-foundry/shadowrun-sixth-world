export default function(params) {
    return {
        name: 'Dice Penalty',
        on: {
            roll(pool) {
                pool.push({
                    text: params.reason,
                    value: params.value,
                });
            }
        }
    }
}