import {object_set, object_get} from "../helpers/objects.js";

export default function(params) {
    return {
        name: 'Example',
        on: {
            calculate(data) {
                console.debug(data);
                // let value = object_get(data, ['conditionMonitor', 'physical', 'penalty'], 0);
                // object_set(data, ['conditionMonitor', 'physical', 'penalty'], value + (params.physical || 0));
            }
        }
    }
}