import {object_get, object_set} from "../helpers/objects.js";

export default function(params) {
    return {
        name: `Attribute boost ${params.attribute}`,
        on: {
            calculate(data) {
                let value = object_get(data, ['attributes', params.attribute, 'modifier'], 0);
                value += params.value;
                object_set(data, ['attributes', params.attribute, 'modifier'], value);
            }
        }
    }
}