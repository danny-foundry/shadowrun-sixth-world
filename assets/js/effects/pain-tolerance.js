import {object_get, object_set} from "../helpers/objects.js";

export default function(params) {
    return {
        name: 'Pain Tolerance',
        on: {
            calculate(data) {
                // todo - check that this is how pain tolerance actually works
                let value = object_get(data, ['condition_monitor', 'physical', 'penalty'], 0);
                value = Math.min(0, value + (params.physical || 0));
                object_set(data, ['condition_monitor', 'physical', 'penalty'], value);

                value = object_get(data, ['condition_monitor', 'stun', 'penalty'], 0);
                value = Math.min(0, value + (params.stun || 0));
                object_set(data, ['condition_monitor', 'stun', 'penalty'], value);
            }
        }
    }
}