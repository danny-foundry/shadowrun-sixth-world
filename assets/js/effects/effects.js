import exampleEffect from "./example-effect.js";
import dicePenalty from "./dice-penalty.js";
import painTolerance from "./pain-tolerance.js";
import attributeBoost from "./attribute-boost.js";

export const allEffects = {
    'dice-penalty': dicePenalty,
    'example': exampleEffect,
    'pain-tolerance': painTolerance,
    'attribute-boost': attributeBoost,
};

export function applyEffects(actor) {
    Object.entries(getModifiersByCharacteristic(actor.items)).forEach(([characteristic, modifier]) => {
        actor.setModifier(characteristic, modifier);
    });
    triggerEffects(actor, 'calculate', actor.data.data);
}

export function triggerEffects(actor, event, ...args) {
    getEffects(actor.items).forEach((effect) => {
        if (effect.on[event]) {
            effect.on[event](...args);
        }
    });
}

export function getEffects(items) {
    return items
                .filter((item) => item.data.data.equipped === undefined || item.data.data.equipped)
                .map((item) => item.data.data.effects)
                .filter((effects) => effects)
                .flat()
                .map((effects) => effects.split("\n"))
                .flat()
                .map((effect) => {
                    try {
                        const model = JSON.parse(effect);

                        if (!allEffects[model.name]) {
                            console.error(`Unknown effect '${model.name}`);
                            return null;
                        }
                        return allEffects[model.name](model);
                    } catch(err) {
                        console.error(err);
                        console.error(effect);
                    }
                })
                .filter((effect) => effect);
}

export function getModifiers(items) {
    return items
        .filter((item) => {
            return item.data.data.equipped === undefined || item.data.data.equipped;
        })
        .map((item) => item.data.data.modifiers)
        .filter((modifiers) => modifiers)
        .flat()
        .map((modifiers) => modifiers.split("\n"))
        .flat();
}

export function getModifiersByCharacteristic(items) {
    let modifiersByCharacteristic = {};

    getModifiers(items)
        .map((item) => {
            let [characteristic, modifier] = item.split("=");
            return {
                characteristic,
                modifier
            }
        })
        .forEach((modifier) => {
            if (!modifiersByCharacteristic[modifier.characteristic]) {
                modifiersByCharacteristic[modifier.characteristic] = 0;
            }
            modifiersByCharacteristic[modifier.characteristic] += parseFloat(modifier.modifier);
        });
    return modifiersByCharacteristic;
}

export function getTotalModifier(characteristic, items) {
    return Math.min(4, this.getModifiersByCharacteristic(items, characteristic) || 0);
}

// effects
// modifiers

