import {RollConfiguration, rollForActor} from "./dice.js";

const buttons = {
    createButtons($doc, actor) {
        $doc.on('click', '[data-action="add-item"]', async function() {
            let type = $(this).data('item-type');

            let data = {
                name: 'Untitled',
                type: type,
            };

            (await actor.createEmbeddedDocuments("Item", [data]))[0].sheet.render(true);
        })
    },
    editButtons($doc, actor) {
        $doc.find('[data-action="edit-item"]').click(function() {
            const itemId = $(this).data('item-id');
            actor.getEmbeddedCollection("Item").get(itemId).sheet.render(true);
        });
    },
    deleteButtons($doc, actor) {
        const self = this;

        $doc.find('[data-action="delete-item"]').click(async function() {
            const itemId = $(this).data('item-id');
            const name = actor.getEmbeddedCollection("Item").get(itemId).data.name;

            // todo - confirmation dialog
            const confirmed = true;
            // const confirmed = await self.confirm(`Delete '${name}'?`, 'systems/gurps-4/templates/dialogs/confirm.html', {
            //     name: name,
            //     action: 'delete',
            // });

            if (confirmed) {
                await actor.deleteEmbeddedDocuments("Item", [itemId]);
            }
        });
    },
    rollButtons($doc, actor) {
        $doc.find('[data-action="roll-initiative"]').click(async function(ev) {
            const dicePool = `${actor.data.data.initiative.dice.total}d6 + ${actor.data.data.initiative.modifier.total}`;
            const results = await new Roll(dicePool).roll();
            const output = `
                <div>
                    Initiative: ${dicePool}
                </div>
                <strong>${results.total}</strong>
            `;

            CONFIG.ChatMessage.documentClass.create({
                user: game.user.id,
                speaker: ChatMessage.getSpeaker({ actor: actor }),
                type: CONST.CHAT_MESSAGE_TYPES.IC,
                flavor: `${output}`,
                sound: CONFIG.sounds.dice,
            });
        });

        $doc.find('[data-action="roll"]').click(async function(ev) {
            let pool = $(this).attr('data-pool');
            let text = $(this).attr('data-text');
            let wildDice = parseInt($(this).attr('data-wild') || 0);
            let twosGlitch = $(this).attr('data-twos-glitch') !== undefined;
            let specialty = $(this).attr('data-specialty') !== undefined;
            let expertise = $(this).attr('data-expertise') !== undefined;
            let ignoreWoundPenalties = $(this).attr('data-ignore-wounds') !== undefined;
            let untrained = $(this).attr('data-untrained') !== undefined;

            let config = new RollConfiguration(wildDice, ignoreWoundPenalties, specialty, expertise, twosGlitch, untrained);

            rollForActor(actor, pool, text, config, ev.shiftKey);
        });
    }
};

function bindButtons($doc, object)
{

}

export function bindActorButtons($doc, actor)
{
    bindButtons($doc, actor);
    buttons.createButtons($doc, actor);
    buttons.editButtons($doc, actor);
    buttons.deleteButtons($doc, actor);
    buttons.rollButtons($doc, actor);
}