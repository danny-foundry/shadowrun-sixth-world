import {formToObject} from "./objects.js";

function dialogForm(title, template, data = {})
{
    return new Promise((resolve, reject) => {
        $.get(template, async (content) => {
            let compiled = Handlebars.compile(content);
            await Dialog.prompt({
                title: title,
                content: compiled(data),
                callback: async (dialog) => {
                    resolve(formToObject($('form', dialog)));
                }
            });
        });
    });
}

async function advancedRollOptions(config, actor)
{
    let characteristics = [];

    actor.attributes().forEach((attr) => {
        characteristics.push({
            name: attr,
            id: `attributes.${attr}`,
        });
    });

    actor.skills()?.forEach((skill) => {
        characteristics.push({
            name: skill.name,
            id: `skills.${skill.name}`
        })
    });

    let optionsForm = await dialogForm('Roll options', '/systems/shadowrun-sixth-world/assets/templates/dialog/advanced-roll.html', { options: config, characteristics: characteristics, });
    config.wildDice = parseInt(optionsForm.wildDice);
    config.sixesExplode = optionsForm.sixesExplode === '1';
    config.twosGlitch = optionsForm.twosGlitch === '1';
    config.ignoreWoundPenalties = optionsForm.ignoreWoundPenalties === '1';
    config.modifier = parseInt(optionsForm.modifier);
    config.specialty = optionsForm.specialty === '1';
    config.expertise = optionsForm.expertise === '1';
    config.characteristicsPool = actor.dicePool(optionsForm.characteristics_pool.filter((characteristic) => characteristic).join(" + "));
    config.untrained = optionsForm.untrained === '1';

    return config;
}

export function RollConfiguration(wildDice = 0, ignoreWoundPenalties = false, specialty = false, expertise = false, twosGlitch = false, untrained = false)
{
    this.wildDice = wildDice;
    this.ignoreWoundPenalties = ignoreWoundPenalties;
    this.specialty = specialty;
    this.expertise = expertise;
    this.twosGlitch = twosGlitch;
    this.modifier = 0;
    this.characteristicsPool = [];
    this.constantsPool = [];
    this.untrained = untrained;

    this.pool = () => [
        ...this.characteristicsPool,
        ...this.constantsPool,
    ];
}

export function parseDicePool(pool)
{
    let parts = [];
    let expr = /[+\-]/;
    let operation = '+';
    let symbol = '';

    while(symbol = expr.exec(pool)) {
        let val = pool.substring(0, pool.indexOf(symbol));
        pool = pool.substring(pool.indexOf(symbol) + 1);
        parts.push(`${operation}${val}`)
        operation = symbol;
    }
    parts.push(`${operation}${pool}`);

    return parts;
}

export function describeRoll(name, dicePool)
{
    let components = [];
    let total = 0;

    dicePool.forEach((element) => {
        let value = element.value;

        if (!value) {
            components.push(element.text);
            return;
        }

        total += value;

        if (value >= 0) {
            value = `+${value}`;
        }
        components.push(`${element.text} ${value}`);
    });

    return `<strong>Rolling ${name}</strong><br/>${components.join('<br/>')}<br/><strong>${total} dice</strong>`;
}

function diceMarkup(dice, fivesNegated, config)
{
    let dieText = '';

    dice.forEach((die, index) => {
        let classes = 'die';

        if (index < config.wildDice) {
            classes += ' die-wild';
        } else {
            if (die === 1 || (die === 2 && config.twosGlitch)) {
                classes += ' die-glitch';
            }
            if (die === 6 || (die === 5 && !fivesNegated)) {
                classes += ' die-hit';
            }
        }

        dieText += `<span class="${classes}">${die}</span>`;
    });

    return dieText;
}

export async function rollForActor(actor, pool, text, config, advanced = false)
{
    let dicePool = actor.dicePool(pool);
    config.characteristicsPool = dicePool.filter((pool) => pool.source.startsWith('attributes.') || pool.source.startsWith('skills.'));
    config.constantsPool = dicePool.filter((pool) => !pool.source.startsWith('attributes.') && !pool.source.startsWith('skills.'));

    if (advanced) {
        config = await advancedRollOptions(config, actor);
    }

    dicePool = config.pool();
    if (config.modifier !== 0) {
        dicePool.push({
            text: 'Custom modifier',
            value: config.modifier,
        })
    }

    if (!config.ignoreWoundPenalties && actor.woundPenalties() < 0) {
        dicePool.push({
            text: 'Wound Penalties',
            value: actor.woundPenalties(),
        });
    }

    if (config.untrained) {
        dicePool.push({
            text: 'Untrained',
            value: -1,
        })
    }

    if (config.specialty) {
        dicePool.push({
            text: 'Specialty',
            value: 2
        });
    }

    if (config.expertise) {
        dicePool.push({
            text: 'Expertise',
            value: 3
        });
    }

    if (config.sixesExplode) {
        dicePool.push({
            text: 'Sixes explode',
        });
    }

    if (config.twosGlitch) {
        dicePool.push({
            text: 'Two\'s glitch',
        });
    }

    if (config.ignoreWoundPenalties) {
        dicePool.push({
            text: 'Ignoring wound penalties',
        });
    }

    if (config.wildDice) {
        dicePool.push({
            text: `${config.wildDice} wild di(c)e`,
        });
    }

    text = describeRoll(text, dicePool);
    let results = await roll(dicePool, config);
    let resultText = '';
    let dieText = '';

    if (results.glitch && results.successes) {
        resultText = '<span class="roll-glitch">Glitch</span>';
    } else if (results.glitch && !results.successes) {
        resultText = '<span class="roll-critical-glitch">Critical Glitch</span>';
    } else if (results.successes) {
        resultText = `<span class="roll-success">Success (${results.successes} hits)</span>`;
    } else {
        resultText = `<span class="roll-failed">Failed</span>`;
    }

    // todo - add colouring to the dice (red for glitch die, green for success die, purple for wild die?)1
    dieText = diceMarkup(results.dice, results.fivesNegated, config);

    CONFIG.ChatMessage.documentClass.create({
        user: game.user.id,
        speaker: ChatMessage.getSpeaker({ actor: this }),
        type: CONST.CHAT_MESSAGE_TYPES.IC,
        flavor: `${text}`,
        content: `<div>${resultText}</div><div>${dieText}</div>`,
        sound: CONFIG.sounds.dice,
    });
}

async function rollPool(pool, config, output, ignoreGlitches = false)
{
    let sixes = 0;

    const results = await new Roll(`${pool}d6`).roll();
    results.dice[0].results.forEach((dice) => {
        output.dice.push(dice.result);
        output.count[dice.result]++;
        if (dice.result === 6) {
            sixes ++;
        }
    });

    output.successes = output.count[5] + output.count[6];

    for (let i = 0; i < config.wildDice; i ++) {
        if (output.dice[i] === 1 && !ignoreGlitches) {
            output.fivesNegated = true;
        }
        if (output.dice[i] === 5) {
            output.wildFives ++;
            output.successes += 2;
        }
        if (output.dice[i] === 6) {
            output.wildSixes ++;
            output.successes += 2;
        }
    }

    if (output.fivesNegated) {
        output.successes -= output.count[5] + (output.wildFives * 2);
    }

    let glitches = ignoreGlitches ? 0 : output.count[1];
    if (config.twosGlitch && !ignoreGlitches) {
        glitches += output.count[2];
    }

    if (glitches >= pool / 2) {
        output.glitch = true;
    }

    if (config.sixesExplode && sixes > 0) {
        return await rollPool(sixes, config, output, true);
    }

    return output;
}

export async function roll(dicePool, config)
{
    // roll dice, count success and glitches
    // re-roll dice if appropriate (wild die)
    let pool = dicePool.reduce((previous, val) => val.value ? previous + val.value : previous, 0);
    if (isNaN(pool)) {
        console.error('Dice Pool NaN:');
        console.error(dicePool);
        return;
    }

    if (pool < 1) {
        pool = 1;
    }


    let output = {
        successes: 0,
        fivesNegated: false,
        wildFives: 0,
        wildSixes: 0,
        glitch: false,
        count:{
            1:0,
            2:0,
            3:0,
            4:0,
            5:0,
            6:0,
        },
        dice: []
    };

    return await rollPool(pool, config, output, false);
}