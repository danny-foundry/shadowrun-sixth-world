export default function() {
    Handlebars.registerHelper('partial', function(value, opt) {
        try {
            return Handlebars.partials['systems/shadowrun-sixth-world/assets/templates/' + value](opt.data.root);
        } catch (err) {
            console.error(`Failed to load systems/shadowrun-sixth-world/assets/templates/${value}`);
            console.error(err);
            return '';
        }
    });

    Handlebars.registerHelper('underscore', function(name) {
        return name.replace(/ /g, '_');
    });

    Handlebars.registerHelper('rigger_attribute', function(attribute) {
        if (attribute === 'Body') {
            return 'Willpower';
        }
        if (attribute === 'Strength') {
            return 'Charisma';
        }
        if (attribute === 'Agility') {
            return 'Logic';
        }
        if (attribute === 'Reaction') {
            return 'Intuition';
        }
        return attribute;
    });

    Handlebars.registerHelper('is_untrained', function(actor, skill, value) {
        if (actor.document.isUntrained(skill)) {
            return value;
        }
        return '';
    });

    Handlebars.registerHelper('gearTable', function(type, items, isEditable) {
        if (!items || !items.length) {
            return '';
        }

        const path = `systems/shadowrun-sixth-world/assets/templates/character/partials/gear-table/${type}.html`;

        try {
            return Handlebars.partials[path]({ items: items, isEditable: isEditable });
        } catch (err) {
            console.error(`Failed to load ${path}`);
            console.error(err);
            return '';
        }
    });

    Handlebars.registerHelper('signed', function(value, opt) {
        if (value < 0) {
            return value;
        }

        return `+${value}`;
    });

    Handlebars.registerHelper('not-disabled', function(val) {
        return val ? '' : 'disabled'
    });

    Handlebars.registerHelper('disabled', function(val) {
        return val ? 'disabled' : ''
    });

    Handlebars.registerHelper('prop', function(item, prop) {
        return item[prop];
    });

    Handlebars.registerHelper('rollString', function(item) {
        return item.getRoll();
    });

    Handlebars.registerHelper('json', function(object) {
        return JSON.stringify(object);
    });
}