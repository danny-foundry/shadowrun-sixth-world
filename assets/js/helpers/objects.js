export function object_get(data, keys, defaultValue = 0)
{
    let key = keys.shift().trim();

    if (!data[key]) {
        return 0;
    }

    if (keys.length === 0) {
        return data[key] || defaultValue;
    }

    return object_get(data[key], keys, defaultValue);
}

export function object_set(data, keys, value)
{
    let key = keys.shift().trim();

    if (keys.length === 0) {
        data[key] = value;
        return;
    }

    if (!data[key]) {
        data[key] = {};
    }

    object_set(data[key], keys, value);
}

export function formToObject($el) {
    var self = $el,
        json = {},
        push_counters = {},
        patterns = {
            "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
            "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
            "push":     /^$/,
            "fixed":    /^\d+$/,
            "named":    /^[a-zA-Z0-9_]+$/
        };


    self.build = function(base, key, value){
        base[key] = value;
        return base;
    };

    self.push_counter = function(key){
        if(push_counters[key] === undefined){
            push_counters[key] = 0;
        }
        return push_counters[key]++;
    };

    $.each($(self).serializeArray(), function(){

        // Skip invalid keys
        if(!patterns.validate.test(this.name)){
            return;
        }

        var k,
            keys = this.name.match(patterns.key),
            merge = this.value,
            reverse_key = this.name;

        while((k = keys.pop()) !== undefined){

            // Adjust reverse_key
            reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

            // Push
            if(k.match(patterns.push)){
                merge = self.build([], self.push_counter(reverse_key), merge);
            }

            // Fixed
            else if(k.match(patterns.fixed)){
                merge = self.build([], k, merge);
            }

            // Named
            else if(k.match(patterns.named)){
                merge = self.build({}, k, merge);
            }
        }

        json = $.extend(true, json, merge);
    });

    return json;
}