export default class QualitySheet extends ItemSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [{ navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-1" }], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.object.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/shadowrun-sixth-world/assets/templates/item-limited.html";
        }
        // use a full editable sheet
        return "systems/shadowrun-sixth-world/assets/templates/quality.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        return {
            item: this.object.data,
            actor: this.object.actor,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);

        // bind any dom listeners that apply to all sheets
        if (!this.isEditable) {
            return;
        }

        // bind any dom listeners that apply to editable sheets
    }
}