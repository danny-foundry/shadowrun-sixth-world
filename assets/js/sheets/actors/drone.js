import {bindActorButtons} from "../../helpers/buttons.js";

export default class ShadowrunDroneSheet extends ActorSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [
                { navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-detail" },
                { navSelector: ".sub-tabs", contentSelector: ".sheet-sub-body", initial: "tab-combat" },
                { navSelector: ".item-tabs", contentSelector: ".sheet-items-body", initial: "tab-skills" },
            ], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.actor.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/shadowrun-sixth-world/assets/templates/character-limited.html";
        }
        // use a full editable sheet
        return "systems/shadowrun-sixth-world/assets/templates/drone.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        let attributes = [
            [
                { name: "Onroad", key: "OnroadHandling", value: this.actor.data.data.attributes.OnroadHandling },
                { name: "Offroad", key: "OffroadHandling", value: this.actor.data.data.attributes.OffroadHandling },
                { name: "Acceleration", key: "Acceleration", value: this.actor.data.data.attributes.Acceleration },
                { name: "Speed Interval", key: "SpeedInterval", value: this.actor.data.data.attributes.SpeedInterval },
            ],
            [
                { name: "Top Speed", key: "TopSpeed", value: this.actor.data.data.attributes.TopSpeed },
                { name: "Body", key: "Body", value: this.actor.data.data.attributes.Body },
                { name: "Armor", key: "Armor", value: this.actor.data.data.attributes.Armor },
                { name: "Pilot", key: "Pilot", value: this.actor.data.data.attributes.Pilot },
            ],
            [
                { name: "Sensor", key: "Sensor", value: this.actor.data.data.attributes.Sensor },
                { name: "Seats", key: "Seats", value: this.actor.data.data.attributes.Seats },
            ]
        ];

        let rigger = this.actor.linkableActorsByType('character')
            .find((char) => char.data.data.rigging.drone === this.actor.id);

        return {
            actor: this.actor.data,
            attributes: attributes,
            items: this.actor.itemsByType(),
            weapons: this.actor.itemsByType()['rangedWeapon'],
            initiative: {
                value: this.actor.data.data.attributes.Pilot.total * 2,
                dice: '3d6',
            },
            isEditable: this.isEditable,
            rigger,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);
        if (!this.isEditable) {
            return;
        }

        const sheet = this;

        bindActorButtons($doc, sheet.actor);
        $doc.on('click', '[data-action="dump"]', () => {
            console.log(sheet.actor.data);
        });
    }
}