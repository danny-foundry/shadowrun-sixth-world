import {bindActorButtons} from "../../helpers/buttons.js";

export default class ShadowrunCharacterSheet extends ActorSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [
                { navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-detail" },
                { navSelector: ".sub-tabs", contentSelector: ".sheet-sub-body", initial: "tab-combat" },
                { navSelector: ".item-tabs", contentSelector: ".sheet-items-body", initial: "tab-skills" },
            ], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.actor.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/shadowrun-sixth-world/assets/templates/character-limited.html";
        }
        // use a full editable sheet
        return "systems/shadowrun-sixth-world/assets/templates/character.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        let attributes = [
            [
                { name: "Body", value: this.actor.data.data.attributes.Body },
                { name: "Agility", value: this.actor.data.data.attributes.Agility },
                { name: "Reaction", value: this.actor.data.data.attributes.Reaction },
                { name: "Strength", value: this.actor.data.data.attributes.Strength },
            ],
            [
                { name: "Willpower", value: this.actor.data.data.attributes.Willpower },
                { name: "Logic", value: this.actor.data.data.attributes.Logic },
                { name: "Intuition", value: this.actor.data.data.attributes.Intuition },
                { name: "Charisma", value: this.actor.data.data.attributes.Charisma },
            ],
        ];

        let specialAttributes = [];
        switch(this.actor.data.data.magic) {
            case "magic":
                specialAttributes.push({ name: 'Magic', value: this.actor.data.data.attributes.Magic });
                break;
            case "resonance":
                specialAttributes.push({ name: 'Resonance', value: this.actor.data.data.attributes.Resonance });
                break;
        }
        specialAttributes.push({ name: 'Edge', value: this.actor.data.data.attributes.Edge });
        specialAttributes.push({ name: 'Essence', value: this.actor.data.data.attributes.Essence, readonly: true });
        attributes.push(specialAttributes);

        const weapons = this.actor.items
            .filter((item) => item.attack_rating !== undefined && item.damage_value !== undefined && item.attack_rating !== null && item.damage_value !== null)
            .sort((a, b) => {
                const aName = a.data.name.toLowerCase();
                const bName = b.data.name.toLowerCase();

                if (aName < bName) {
                    return -1;
                }
                if (aName > bName) {
                    return 1;
                }
                return 0;
            })
            .map((item) => {
                const unequipped = item.data.data.equipped !== undefined && !item.data.data.equipped;

                item.data.data.derived.is_unequipped = unequipped;
                item.data.data.derived.is_rollable = !!(item.attack_skill && !unequipped);
                item.data.data.derived.is_not_rollable = !(item.attack_skill && !unequipped);
                item.data.data.derived.classes = [];

                if (unequipped) {
                    item.data.data.derived.classes.push('unequipped');
                }

                return item;
            });

        let gear = {};

        this.actor.items
            .filter((item) => ['meleeWeapon', 'rangedWeapon', 'equipment', 'spell', 'bioware', 'cyberware', 'armour'].includes(item.data.type))
            .sort((a, b) => {
                const aName = a.data.name.toLowerCase();
                const bName = b.data.name.toLowerCase();

                if (aName < bName) {
                    return -1;
                }
                if (aName > bName) {
                    return 1;
                }
                return 0;
            })
            .map((item) => {
                const unequipped = item.data.data.equipped !== undefined && !item.data.data.equipped;
                item.data.data.derived.is_unequipped = unequipped;

                item.data.data.derived.classes = [];

                if (unequipped) {
                    item.data.data.derived.classes.push('unequipped');
                }
                return item;
            })
            .forEach((item) => {
                if (!gear[item.data.type]) {
                    gear[item.data.type] = [];
                }
                gear[item.data.type].push(item);
            });

        let isRigging = this.actor.data.data.settings.rigger && this.actor.data.data.rigging.drone;
        let drone = null;
        let droneWeapons = [];
        let engineering = this.actor.itemsByType()['skill']?.find((skill) => skill.name === 'Engineering');
        let hasGunnerySpecialty = engineering?.data.data.specialty === 'Gunnery';
        let hasGunneryExpertise = engineering?.data.data.expertise === 'Gunnery';
        let speedPenalty = 0;
        let handlingThreshold = 0;

        if (isRigging) {
            drone = Actors.instance.get(this.actor.data.data.rigging.drone);
            if (!drone) {
                isRigging = false;
            } else {
                droneWeapons = drone.itemsByType()['rangedWeapon'];
            }
            speedPenalty = drone.data.data.speed_penalty;
            handlingThreshold = drone.data.data.attributes[drone.data.data.handling].total;
        }

        return {
            actor: this.actor.data,
            riggable: this.actor.linkableActorsByType('drone'),
            rigging: isRigging,
            drone: drone,
            droneWeapons: droneWeapons,
            handlingThreshold,
            hasGunnerySpecialty,
            hasGunneryExpertise,
            speedPenalty,
            items: this.actor.itemsByType(),
            isEditable: this.isEditable,
            attributes: attributes,
            weapons: weapons,
            gear: gear,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);
        if (!this.isEditable) {
            return;
        }

        const sheet = this;

        bindActorButtons($doc, sheet.actor);
        $doc.on('click', '[data-action="dump"]', () => {
            console.log(sheet.actor.data);
        });

        $doc.on('change', '[data-linked]', (evt) => {
            const id = evt.target.getAttribute('data-linked');
            const attribute = evt.target.getAttribute('data-attribute');
            const entity = evt.target.getAttribute('data-entity');
            let model;

            if (entity === 'Actor') {
                model = Actors.instance.get(id);
            } else {
                model = Items.instance.get(id);
            }

            if (!model) {
                return;
            }

            let updates = {};
            updates[attribute] = evt.target.value;

            model.update(updates);
            setTimeout(() => {
                sheet.render();
            }, 100);
        });
    }
}