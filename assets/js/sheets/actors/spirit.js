import {bindActorButtons} from "../../helpers/buttons.js";

export default class ShadowrunSpiritSheet extends ActorSheet {
    /* Setup sheet options */
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            tabs: [
                { navSelector: ".tabs", contentSelector: ".sheet-body", initial: "tab-detail" },
                { navSelector: ".sub-tabs", contentSelector: ".sheet-sub-body", initial: "tab-combat" },
                { navSelector: ".item-tabs", contentSelector: ".sheet-items-body", initial: "tab-skills" },
            ], // setup tabbed navigation
        });
    }

    /* Return the template file to use for rendering the sheet */
    /** @override */
    get template() {
        let isGm = game.user.isGM;
        let isLimited = this.actor.limited; // permissions

        if (isLimited) {
            // use a sheet that shows only the description
            return "systems/shadowrun-sixth-world/assets/templates/character-limited.html";
        }
        // use a full editable sheet
        return "systems/shadowrun-sixth-world/assets/templates/spirit.html";
    }

    /* Get data for the view each key in the returned object is available as a variable in the template */
    /** @override */
    getData() {
        let attributes = [
            [
                { name: "Body", value: this.actor.data.data.attributes.Body },
                { name: "Agility", value: this.actor.data.data.attributes.Agility },
                { name: "Reaction", value: this.actor.data.data.attributes.Reaction },
                { name: "Strength", value: this.actor.data.data.attributes.Strength },
            ],
            [
                { name: "Willpower", value: this.actor.data.data.attributes.Willpower },
                { name: "Logic", value: this.actor.data.data.attributes.Logic },
                { name: "Intuition", value: this.actor.data.data.attributes.Intuition },
                { name: "Charisma", value: this.actor.data.data.attributes.Charisma },
            ],
        ];

        let specialAttributes = [];
        switch(this.actor.data.data.magic) {
            case "magic":
                specialAttributes.push({ name: 'Magic', value: this.actor.data.data.attributes.Magic });
                break;
            case "resonance":
                specialAttributes.push({ name: 'Resonance', value: this.actor.data.data.attributes.Resonance });
                break;
        }
        specialAttributes.push({ name: 'Edge', value: this.actor.data.data.attributes.Edge });
        specialAttributes.push({ name: 'Essence', value: this.actor.data.data.attributes.Essence, readonly: true });
        attributes.push(specialAttributes);

        const weapons = this.actor.items
            .filter((item) => item.data.data.attack_rating !== undefined && item.data.data.damage_value !== undefined)
            .map((item) => {
                const unequipped = item.data.data.equipped !== undefined && !item.data.data.equipped;

                item.data.data.derived.is_unequipped = unequipped;
                item.data.data.derived.is_rollable = !!(item.attack_skill && !unequipped);
                item.data.data.derived.is_not_rollable = !(item.attack_skill && !unequipped);
                item.data.data.derived.classes = [];

                if (unequipped) {
                    item.data.data.derived.classes.push('unequipped');
                }

                return item;
            });

        let gear = {};

        this.actor.items
            .filter((item) => ['meleeWeapon', 'rangedWeapon', 'equipment', 'spell', 'bioware', 'cyberware'].includes(item.data.type))
            .map((item) => {
                const unequipped = item.data.data.equipped !== undefined && !item.data.data.equipped;
                item.data.data.derived.is_unequipped = unequipped;

                item.data.data.derived.classes = [];

                if (unequipped) {
                    item.data.data.derived.classes.push('unequipped');
                }
                return item;
            })
            .forEach((item) => {
                if (!gear[item.data.type]) {
                    gear[item.data.type] = [];
                }
                gear[item.data.type].push(item);
            });

        return {
            actor: this.actor.data,
            items: this.actor.itemsByType(),
            isEditable: this.isEditable,
            attributes: attributes,
            weapons: weapons,
            gear: gear,
        };
    }

    /* Bind UI listeners */
    /** @override */
    activateListeners($doc) {
        super.activateListeners($doc);
        if (!this.isEditable) {
            return;
        }

        const sheet = this;

        bindActorButtons($doc, sheet.actor);
        $doc.on('click', '[data-action="dump"]', () => {
            console.log(sheet.actor.data);
        });

        // todo - move this somewhere more generic
        // $doc.on('click', '[data-action="roll"]', async function() {
        //     let pool = $(this).attr('data-pool');
        //     let text = $(this).attr('data-text');
        //     let wildDice = parseInt($(this).attr('data-wild') || 0);
        //     let twosGlitch = $(this).attr('data-twos-glitch') !== undefined;
        //     let specialty = $(this).attr('data-specialty') !== undefined;
        //     let expertise = $(this).attr('data-expertise') !== undefined;
        //     let ignoreWoundPenalties = $(this).attr('data-ignore-wounds') !== undefined;
        //
        //     let config = new RollConfiguration(wildDice, ignoreWoundPenalties, specialty, expertise, twosGlitch);
        //
        //     rollForActor(sheet.actor, pool, text, config);
        // });
    }
}